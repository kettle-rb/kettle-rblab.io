---
# front matter tells Jekyll to process Liquid
---
<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://kettle-rb.gitlab.io/assets/img/logos/">kette-rb logos</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://railsbling.com">Peter Boling</a> are licensed under <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p>

<ul>
    {% assign image_files = site.static_files | where: "image", true %}
    {% for myimage in image_files %}
        <li>
            <a href="{{ myimage.path }}">
                {% assign image_ext = myimage.path | split: "." | last %}
                {% if image_ext == "png" %}
                    <img height="200px" alt="kettle-rb logo, Copyright (c) 2023 Peter Boling, CC BY-SA 4.0" src="{{ myimage.path }}"/>
                {% elsif image_ext == "svg" %}
                    <object data="{{ myimage.path }}" height="100px"> </object>
                {% else %}
                  Please report a bug regarding {{ myimage.path }} to <a href="https://gitlab.com/kettle-rb/kettle-rb.gitlab.io/-/issues">gitlab.com/kettle-rb/kettle-rb.gitlab.io/-/issues</a>
                {% endif %}
            </a>
        </li>
    {% endfor %}
</ul>
